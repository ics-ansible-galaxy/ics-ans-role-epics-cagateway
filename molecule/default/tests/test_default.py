import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_carepeater_enabled_and_started(host):
    service = host.service("carepeater")
    assert service.is_running
    assert service.is_enabled


def test_cagateway_enabled_and_started(host):
    service = host.service("cagateway")
    assert service.is_running
    assert service.is_enabled


def test_gateway_pvlist(host):
    gateway_pvlist = host.file("/etc/gateway/gateway.pvlist")
    assert r"""EVALUATION ORDER ALLOW,DENY

.* ALLOW
.*\$.* DENY""" in gateway_pvlist.content_string


def test_gateway_access(host):
    gateway_access = host.file("/etc/gateway/gateway.access")
    if host.ansible.get_variables()["inventory_hostname"] == "ics-ans-role-epics-cagateway-default":
        assert "WRITE" not in gateway_access.content_string
        assert r"""ASG(DEFAULT) {
   RULE(1,READ)
}""" in gateway_access.content_string
    else:
        assert r"""ASG(DEFAULT) {
   RULE(1,READ)
   RULE(1,WRITE,TRAPWRITE)
}""" in gateway_access.content_string
