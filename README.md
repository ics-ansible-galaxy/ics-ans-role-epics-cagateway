# ics-ans-role-epics-cagateway

Ansible role to install epics-cagateway.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
epics_cagateway_version: 2.1.2.0
# User to run the channel access gateway
epics_cagateway_user: cagw

# Host specific variables
# epics_cagateway_write = all, specific or false
epics_cagateway_write: false
epics_cagateway_client_broadcast: 127.255.255.255
epics_cagateway_server_ip: 127.0.0.1

# In case of loop declare IP address to ignore
epics_cagateway_ignore_ip: 127.0.0.2

# Required static routes
epics_cagateway_static_routes: []
# Should be a list of dictionaries as below:
# epics_cagateway_static_routes:
#   - dev: eth1
#     routes:
#       - 15.15.0.0/24 via 10.1.1.110
#       - 192.168.0.0/24 via 10.1.1.110

epics_cagateway_commands:
  - R1
  - R2
  - R3
  - AS

epics_cagateway_pvlist_evaluation_order: "ALLOW,DENY"
# By default allow all PV names except the ones including a $
# The $ sign denotes an unresolved macro (resulting in an invalid PV name)
epics_cagateway_pvlist_rules:
  - '.* ALLOW'
  - '.*\$.* DENY'

# User access group and associated users
epics_cagateway_uag:
  - name: group1
    users:
      - user1
      - user2
  - name: group2
    users:
      - userx
      - usery
# host access group and associated hosts
epics_cagateway_hag:
  - name: hostgroup
    hosts:
      - workstation1
      - workstation2
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-epics-cagateway
```

License
-------

BSD 2-clause
---
